package com.example.festival;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;

public class FestivalEndpointTest {
    private static final String RESPONSE_BODY_WITH_TEST_EVENT = "[{\"title\": \"Test Event\"}]";
    private static final String APPLICATION_JSON = "application/json;ver=2.0";
    private static final String ACCEPT = "Accept";
    private static final String FESTIVAL_QUERY_REGEX = "/events\\?key=[A-Za-z]{15}&signature=[0-9A-Za-z]{40}";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicHttpsPort());

    private FestivalEndpoint underTest;

    @Before
    public void setUp() {
        underTest = new FestivalEndpoint(Environment.LOCAL);
    }

    @Test
    public void testGetAllEvents() {
        stubFor(get(urlMatching(FESTIVAL_QUERY_REGEX))
            .withHeader(ACCEPT, equalTo(APPLICATION_JSON))
            .willReturn(
                aResponse()
                    .withStatus(200)
                    .withHeader("Content-Type", APPLICATION_JSON)
                    .withBody(RESPONSE_BODY_WITH_TEST_EVENT)
            )
        );

        String responseBody = underTest.getEvents();

        verify(1, getRequestedFor(urlMatching(FESTIVAL_QUERY_REGEX))
            .withHeader(ACCEPT, equalTo(APPLICATION_JSON))
        );

        assertEquals(RESPONSE_BODY_WITH_TEST_EVENT, responseBody);
    }

    @After
    public void tearDown() {
    }
}