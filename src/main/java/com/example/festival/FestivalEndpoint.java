package com.example.festival;

import com.example.festival.utils.APIUriGenerator;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

class FestivalEndpoint {
    private static final Logger logger = Logger.getLogger(FestivalEndpoint.class);

    private Environment env;

    FestivalEndpoint(Environment env) {
        this.env = env;
    }

    String getEvents() {
        logger.info("Fetching all events in " + env + " environment.");
        APIUriGenerator apiUriGenerator = new APIUriGenerator();
        String uri = apiUriGenerator.generateUri(env);

        if (uri == null) return null;

        logger.info("Uri generated. Creating request to send to the Festival API.");
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(uri);
        request.setHeader("Accept", "application/json;ver=2.0");

        return executeRequest(client, request);
    }

    private String executeRequest(HttpClient client, HttpGet request) {
        InputStream inputStream;

        logger.info("Executing request to the Festival API.");
        try {
            HttpResponse httpResponse = client.execute(request);
            inputStream = httpResponse.getEntity().getContent();
        } catch (IOException e) {
            logger.error(e.getMessage(), e.getCause());
            return null;
        }

        logger.info("Response received from the Festival API.");
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        String response = scanner.useDelimiter("\\Z").next();
        scanner.close();

        return response;
    }
}
