package com.example.festival.utils;

import com.example.festival.Environment;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.log4j.Logger;

import static org.apache.commons.codec.digest.HmacAlgorithms.HMAC_SHA_1;

public class APIUriGenerator {

    private static final Logger logger = Logger.getLogger(APIUriGenerator.class);

    public String generateUri(Environment env) {
        logger.info("Generating URI for the Festival API request.");

        String query = "/events?key=" + System.getenv("FESTIVAL_API_KEY");
        byte[] queryBytes = query.getBytes();
        byte[] secretKeyBytes = System.getenv("FESTIVAL_SECRET_KEY").getBytes();
        String signature = calculateSignature(queryBytes, secretKeyBytes);

        query += "&signature=" + signature;

        switch(env) {
            case PROD: return "https://api.edinburghfestivalcity.com" + query;
            default: return "http://localhost:8080" + query;
        }
    }

    private String calculateSignature(byte[] queryBytes, byte[] secretKeyBytes) {
        logger.info("Calculating signature for API call.");

        return new HmacUtils(HMAC_SHA_1, secretKeyBytes).hmacHex(queryBytes);
    }
}
